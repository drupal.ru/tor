### Вопросы и Ответы

Вопросы и Ответы — раздел на сайте, где отображаются вопросы, опубликованные пользователями сайта.

#### Типы сущностей в разделе «Вопросы и Ответы»

##### Вопрос

Тип сущности: Вопрос

Предлагаемые поля:
* Заголовок (title)
* Вопрос (?)

Кейсы:
* Пользователь может задать вопрос (создать сущность типа "Вопрос")
* Пользователь может создать сколь угодно сущностей типа "Вопрос"

##### Ответ

Тип сущности: Ответ

Кейсы:
* Пользователь может написать ответ на вопрос
* Один и тот же пользователь может ответить на вопрос только один раз. Если пользователь захочет что-то добавить, 
он должен отредактировать свой ответ. Это будет сделано для того, чтобы можно было считать в рейтинге пользователя
эффективность (полезность) пользователя: соотношение сколько он дал ответов и сколько из них было выбрано решением
* Автор вопроса или модератор (кто имеет права доступа) могут отметить ответ как решение вопроса
* В настройках модуля должна быть настройка сколько ответов можно отметить как решение: только один или несколько
* Ответы-решения должны быть первыми при отображении всех ответов
* Нужна настройка и права доступа "заблокировать решения". Если модератор заблокировал решения, то автор вопроса 
больше не может отмечать ответы как решения или снимать отметки

##### Комментарий к вопросу

Тип сущности: Комментарий к вопросу

Кейсы:
* Чтобы задать уточняющие вопросы автору поста (вопроса), пользователи могут писать комментарии под вопросом
* Пользователь может создать сколь угодно сущностей типа "Комментарий к вопросу"

##### Комментарий к ответу

Тип сущности: Комментарий к ответу

Кейсы:
* Чтобы задать уточняющие вопросы автору ответа, пользователи могут писать комментарии под ответом
* Пользователь может создать сколь угодно сущностей типа "Комментарий к ответу"
